package com.example.nikit.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.nikit.myapplication.network.NetworkModule;

import java.io.File;

/**
 * Created by nikit on 06.08.2016.
 */
public class Base extends AppCompatActivity {
    Deps deps;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();

    }
    public Deps getDeps() {
        return deps;
    }
}