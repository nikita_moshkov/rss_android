package com.example.nikit.myapplication;

import com.example.nikit.myapplication.main.MainActivity;
import com.example.nikit.myapplication.network.NetworkModule;
import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by nikit on 07.08.2016.
 */
@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(MainActivity mainActivity);
}
