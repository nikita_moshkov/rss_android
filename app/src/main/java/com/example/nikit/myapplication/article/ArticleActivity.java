package com.example.nikit.myapplication.article;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikit.myapplication.Base;
import com.example.nikit.myapplication.R;
import com.example.nikit.myapplication.main.MainPresenter;
import com.example.nikit.myapplication.models.Article;

import javax.inject.Inject;

/**
 * Created by nikit on 11.08.2016.
 */
public class ArticleActivity extends Base implements ArticleView {

    private TextView mTitle;
    private TextView mPubDate;
    private TextView mLink;
    private WebView mDescription;
    private ImageView mImage;

    @Inject
    ArticlePresenter articlePresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        renderView();
        showArticle();

        ArticlePresenter articlePresenter = new ArticlePresenter(this);
        articlePresenter.getArticle();
    }


    public  void renderView(){
        setContentView(R.layout.activity_article);
        mTitle = (TextView) findViewById(R.id.title);
        mPubDate = (TextView) findViewById(R.id.pubDate);
        mLink = (TextView) findViewById(R.id.link);
        mDescription = (WebView) findViewById(R.id.description);
        mImage = (ImageView) findViewById(R.id.image);

    }


    public void showArticle() {
        Article article = (Article) getIntent().getSerializableExtra("article");
        mTitle.setText(Html.fromHtml(article.getmTitle()));
        mPubDate.setText(Html.fromHtml(article.getmPubDate()));
        mLink.setText(Html.fromHtml(article.getmLink()));
        mDescription.loadDataWithBaseURL("", article.getmDescription(), "text/html", "UTF-8", "");
        if (article.getmImage() != "" && article.getmImage() != null) {
            String image = article.getmImage();
            Glide.with(this)
                    .load(image)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(false)
                    .into(mImage);
        }
    }
}