package com.example.nikit.myapplication.article;

import com.example.nikit.myapplication.main.MainView;
import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.network.Service;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by nikit on 11.08.2016.
 */
public class ArticlePresenter{
    private ArticleView articleView;

    public ArticlePresenter(ArticleView articleView) {
        this.articleView = articleView;
    }

    public void getArticle() {
        articleView.showArticle();
    }
}
