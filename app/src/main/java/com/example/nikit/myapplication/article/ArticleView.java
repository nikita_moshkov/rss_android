package com.example.nikit.myapplication.article;

import android.view.View;

import com.example.nikit.myapplication.models.Article;

/**
 * Created by nikit on 11.08.2016.
 */
public interface ArticleView {
    void showArticle();
}

