package com.example.nikit.myapplication.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SlidingPaneLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.support.v7.widget.Toolbar;

import com.example.nikit.myapplication.article.ArticleActivity;
import com.example.nikit.myapplication.Base;
import com.example.nikit.myapplication.R;
import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.models.RssResponse;
import com.example.nikit.myapplication.network.Service;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.inject.Inject;

public class MainActivity extends Base implements MainView {

    private String mCacheId;

    private RecyclerView list;
    private RecyclerView sourcesRecyler;
    @Inject
    public Service service;

    private SlidingPaneLayout mSlidingLayout;

    private Button addSource;

    private MainPresenter presenter;
    private DualCache<String[]> mCache;

    private String[] sources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        CacheSerializer<String[]> jsonSerializer = new JsonSerializer<>(String[].class);
        mCache = new Builder<>(mCacheId, 1, String[].class)
                .useSerializerInRam(50 * 1024, jsonSerializer)
                .useSerializerInDisk(200 * 10214, true, jsonSerializer, getApplicationContext())
                .enableLog()
                .build();

        getDeps().inject(this);
        presenter = new MainPresenter(service, this);
        renderView();
        init();
        presenter.getArticleList(sources);

    }

    public String[] renderView(){
        setContentView(R.layout.activity_main);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        list = (RecyclerView) findViewById(R.id.list);
        sourcesRecyler = (RecyclerView) findViewById(R.id.sources);
        mSlidingLayout = (SlidingPaneLayout) findViewById(R.id.slidingPanel);

        addSource = (Button) findViewById(R.id.button_add);

        LayoutInflater inflater = getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View view=inflater.inflate(R.layout.dialog_add_source, null);
        builder.setView(view);

        getSources();
        final EditText addSourceEditText = (EditText) view.findViewById(R.id.source_link);
        builder.setPositiveButton("Add rss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                String source = addSourceEditText.getText().toString();
                if (mCache.get("source") == null) {
                    ArrayList<String> sourcesList = new ArrayList<>();
                    sourcesList.add(source);
                    String[] sourcesArray = new String[sourcesList.size()];
                    sourcesArray = sourcesList.toArray(sourcesArray);
                    Log.v("s ", sourcesArray.toString());
                    mCache.put("source", sourcesArray);
                    sources = sourcesArray;
                    getSources();
                    presenter.getArticleList(sources);
                }
                else {
                    String[] sourcesArray = mCache.get("source");
                    ArrayList<String> sourcesList = new ArrayList<String>(Arrays.asList(sourcesArray));
                    sourcesList.add(source);
                    sourcesArray = new String[sourcesList.size()];
                    sourcesArray = sourcesList.toArray(sourcesArray);
                    mCache.put("source", sourcesArray);
                    sources = sourcesArray;
                    getSources();
                    presenter.getArticleList(sources);
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();

        addSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.show();
            }
        });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlidingLayout.openPane();
            }
        });



        mSlidingLayout.setPanelSlideListener(panelListener);
        mSlidingLayout.closePane();
        return sources;

    }


    public void init(){
        list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void getRssSuccess(ArrayList<Article> articles) {
        MainAdapter adapter = new MainAdapter(getApplicationContext(), articles,
                new MainAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(Article item) {
                        showArticle(item);
                    }
                });

        list.setAdapter(adapter);
    }

    @Override
    public void getSources() {
        sources = mCache.get("source");
        if (sources == null) sources = new String[0];
        ArrayList<String> sourcesL = new ArrayList<String>();
        sourcesL.addAll(Arrays.asList(sources));
        sourcesRecyler.setLayoutManager(new LinearLayoutManager(this));
        MainSourcesAdapter adapterSources = new MainSourcesAdapter(getApplicationContext(), sources.length != 0 ? sourcesL : new ArrayList<String>());
        sourcesRecyler.setAdapter(adapterSources);
    }

    @Override
    public void showArticle(Article article) {
            Intent i = new Intent (this, ArticleActivity.class);
            i.putExtra("article", article);
            this.startActivity(i);
    }

    SlidingPaneLayout.PanelSlideListener panelListener = new SlidingPaneLayout.PanelSlideListener() {

        @Override
        public void onPanelClosed(View arg0) {

        }

        @Override
        public void onPanelOpened(View arg0) {

        }

        @Override
        public void onPanelSlide(View arg0, float arg1) {

        }
    };

}