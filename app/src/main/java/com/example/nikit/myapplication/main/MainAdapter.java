package com.example.nikit.myapplication.main;

/**
 * Created by nikit on 07.08.2016.
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.nikit.myapplication.R;
import com.example.nikit.myapplication.models.Article;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<Article> data;
    private Context context;

    public MainAdapter(Context context, List<Article> data, OnItemClickListener listener) {
        this.data = data;
        this.listener = listener;
        this.context = context;
    }


    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        Collections.sort(data);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {

        holder.click(data.get(position), listener);
        holder.tvTitle.setText(data.get(position).getmTitle());
        holder.tvDate.setText(data.get(position).getmPubDate());
        Log.v("date", data.get(position).getmPubDate());


        if (data.get(position).getmImage() != "" && data.get(position).getmImage() != null) {
            String image = data.get(position).getmImage();
            Glide.with(context)
                    .load(image)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(false)
                    .into(holder.ivImage);
        } else if (data.get(position).getmImage2() != "" && data.get(position).getmImage2() != null) {
            String image = data.get(position).getmImage2();
            Glide.clear(holder.ivImage);
            Glide.with(context)
                    .load(image)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(false)
                    .into(holder.ivImage);
        }  else {
            Glide.clear(holder.ivImage);
            holder.ivImage.setImageResource(R.drawable.iconscottpilgrim);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onClick(Article Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDate;
        ImageView ivImage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title);
            tvDate = (TextView) itemView.findViewById(R.id.date);
            ivImage = (ImageView) itemView.findViewById(R.id.image);
        }


        public void click(final Article article, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(article);
                }
            });
        }
    }
}