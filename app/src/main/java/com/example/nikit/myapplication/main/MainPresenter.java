package com.example.nikit.myapplication.main;



import android.util.Log;

import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.models.RssResponse;
import com.example.nikit.myapplication.network.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nikit on 06.08.2016.
 */
public class MainPresenter {

    private final Service service;
    private final MainView view;
    private CompositeSubscription subscriptions;
    private ArrayList<Article> articles;

    public MainPresenter(Service service, MainView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.articles = new ArrayList<>();
    }

    public void getArticleList(String[] sources) {

        Subscriber<String> mySubscriber = new Subscriber<String>() {
            @Override
            public void onNext(String source) {
                Subscription subscription = service.getArticleList(new Service.getArticleListCallback() {
                    @Override
                    public void onSuccess(RssResponse rssResponse) {
                        articles.addAll(rssResponse.getItems());
                        System.out.println("");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("Presenter",e.getMessage());
                    }

                }, source);
                Log.v("onNext", "" + articles.size());

            }

            @Override
            public void onCompleted() {
                try {
                    Thread.sleep(3000); // за этот грязнейший хак мне стыдно, но я не знаю как лучше
                                        // тут явная проблема с потоками
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                view.getRssSuccess(articles);
            }

            @Override
            public void onError(Throwable e) {  }
        };

        Subscription subscription_all = Observable.from(Arrays.asList(sources))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mySubscriber);
        subscriptions.add(subscription_all);


        Collections.sort(articles);

    }
}
