package com.example.nikit.myapplication.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.nikit.myapplication.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nikit on 12.08.2016.
 */
public class MainSourcesAdapter extends RecyclerView.Adapter<MainSourcesAdapter.ViewHolder> {
    private ArrayList<String> data;
    private Context context;

    public MainSourcesAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        this.context = context;
    }


    @Override
    public MainSourcesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.source_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MainSourcesAdapter.ViewHolder holder, final int position) {
        holder.tvLink.setText(data.get(position));
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onClick(String Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvLink;

        public ViewHolder(View itemView) {
            super(itemView);
            tvLink = (TextView) itemView.findViewById(R.id.link);
        }

        public void click(final String source, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(source);
                }
            });
        }
    }
}