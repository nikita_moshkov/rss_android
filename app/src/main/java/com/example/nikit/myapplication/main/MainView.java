package com.example.nikit.myapplication.main;

import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.models.RssResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikit on 06.08.2016.
 */
public interface MainView {

    void getRssSuccess(ArrayList<Article> articles);

    void showArticle(Article article);

    void getSources();

}
