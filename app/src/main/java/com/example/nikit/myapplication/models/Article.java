package com.example.nikit.myapplication.models;

import android.media.Image;
import android.support.annotation.Nullable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nikit on 07.08.2016.
 */

@Root(name = "item", strict=false)
@Path("item")
public class Article implements Serializable, Comparable<Article> {
    @Element(name = "title")
    public String mTitle;

    //@Element(name = "link")
    @Path("link")
    @Text(required=false)
    public String mLink = "";
    //public String mLink;

    @Path("description")
    @Text(required=false)
    //@Element(name = "description")
    public String mDescription = "";

    @Element(name = "pubDate")
    public String mPubDate;

    @Path("media:content")
    @Text(required=false)
    @Attribute(name = "url")
    //@Element(name = "media:content")
    private String mImage;

    @Element(name = "description", data = true)
    @Path("img")
    @Attribute(name = "src")
    //@Element(name = "media:content")
    private String mImage2;

    public Article() {}

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmPubDate() {
        return mPubDate;
    }

    public void setmPubDate(String mPubDate) {
        this.mPubDate = mPubDate;
    }

    public String getmLink() {
        return mLink;
    }

    public void setmLink(String mLink) {
        this.mLink = mLink;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String image) {
        this.mImage = image;
    }

    public String getmImage2() {
        return mImage2;
    }

    public void setmImage2(String mImage2) {
        this.mImage2 = mImage2;
    }

    @Override
    public int compareTo(Article o) {
        String stringDateFormat = "EEE, dd MMM yyyy HH:mm:ss z";
        SimpleDateFormat format = new SimpleDateFormat(stringDateFormat, Locale.US);

        long dateOne = 0L;
        long dateTwo = 0L;
        try {

            dateOne = format.parse(getmPubDate()).getTime();
            dateTwo = format.parse(o.getmPubDate()).getTime();
            if (dateOne  < dateTwo)
                return 1;
            if (dateOne  == dateTwo)
                return 0;
            return -1;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}