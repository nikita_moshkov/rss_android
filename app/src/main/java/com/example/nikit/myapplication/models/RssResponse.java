package com.example.nikit.myapplication.models;

import android.media.Image;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikit on 07.08.2016.
 */


@Root(name = "rss", strict=false)
    public class RssResponse {
    @Path("channel")
    @ElementList(entry = "item", inline = true)
    ArrayList<Article> items;


    public RssResponse() {
    }

    public ArrayList<Article> getItems() {
        return items;
    }

    public void setItems(ArrayList<Article> items) {
        this.items = items;
    }
}
