package com.example.nikit.myapplication.network;

import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.models.RssResponse;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by nikit on 07.08.2016.
 */
public interface NetworkService {

    //@GET("Arts.xml")
    @GET
    Observable<RssResponse> getArticleList(@Url String url);
    //Observable<RssResponse> getArticleList();

}