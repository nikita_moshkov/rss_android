package com.example.nikit.myapplication.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikit on 07.08.2016.
 */
public class Response {
    @SerializedName("status")
    public String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public Response() {
    }

    public Response(String status) {
        this.status = status;
    }
}