package com.example.nikit.myapplication.network;

import com.example.nikit.myapplication.models.Article;
import com.example.nikit.myapplication.models.RssResponse;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by nikit on 07.08.2016.
 */
public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getArticleList(final getArticleListCallback callback, String source) {

        return networkService.getArticleList(source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends RssResponse>>() {
                    @Override
                    public Observable<RssResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<RssResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);

                    }

                    @Override
                    public void onNext(RssResponse rssResponse) {
                        callback.onSuccess(rssResponse);
                    }
                });
    }

    public interface getArticleListCallback{
        void onSuccess(RssResponse rssResponse);

        void onError(Throwable e);
    }
}